(in-package :cl-user)
(defpackage hello-world
  (:use :cl))
(in-package :hello-world)

(declaim (optimize (debug 0) (safety 0) (speed 3)))

(defun hello-world-iter (n)
  (loop
     for i from 1 to n 
     do (format t "hello world ~a times!~%" i)))

;;; hello-world-iter output.
;;
;; HELLO-WORLD> (hello-world-iter 10)
;; hello world 1 times!
;; hello world 2 times!
;; hello world 3 times!
;; hello world 4 times!
;; hello world 5 times!
;; hello world 6 times!
;; hello world 7 times!
;; hello world 8 times!
;; hello world 9 times!
;; hello world 10 times!
;; NIL

(defun hello-world-recurse (n)
  (if (= n 0)
      0
      (progn 
        (hello-world-recurse (- n 1))
        (format t "hello world ~a times!~%" n))))

;;; hello-world-recurse output.
;;
;; HELLO-WORLD> (hello-world-recurse 10)
;; hello world 1 times!
;; hello world 2 times!
;; hello world 3 times!
;; hello world 4 times!
;; hello world 5 times!
;; hello world 6 times!
;; hello world 7 times!
;; hello world 8 times!
;; hello world 9 times!
;; hello world 10 times!
;; NIL
