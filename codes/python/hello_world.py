def hello_world_iter(n):
    for i in range(1, n+1):
        print("hello world {} times!".format(i))

def hello_world_recurse(n):
    if n == 0:
        return 0
    else:
        hello_world_recurse(n - 1)
        print("hello world {} times!".format(n))
